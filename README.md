# CS 409 Final Project - Asian Foob Club

## Team members:
1. Nan Liu (nanliu4) 
2. Jiun-An Fang (jafang2)
3. Po-Wen Lin (powenwl2)
4. Ching Chang (chingc4)

### Motivation:
Asian students make up a large body of student population at University of Illinois. Most people are studying away from home for the first time. One important way to alleviate homesickness is to have home food. The goal is to create a platform where all listed restaurants are focused on Asian cuisines, such that students are able to easily locate the local Asian businesses they are interested in helping.

### Demo (https://restaurant-search-28d07.web.app/login):
Our project [web demo](https://restaurant-search-28d07.web.app/login) is up!
Please use the link to access our web demo.


### Video (https://drive.google.com/file/d/1MKKI7DAd04qlS_SeYFzTN200up299cyb/view?usp=share_link)
Our video for going over our project is [here](https://drive.google.com/file/d/1MKKI7DAd04qlS_SeYFzTN200up299cyb/view?usp=share_link)!

Again, thank everybody for the efforts!
